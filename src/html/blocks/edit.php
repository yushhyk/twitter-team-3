<?php ?>

<div class="container edit-main col-lg-10">
    <div class="edit-row row">
        <div class="edit-profile-block col-lg-3">
            <div class="edit-background">
                <div class="edit-profile-img"></div>
            </div>
            <div class="edit-card">
                <div class="edit-username">
                    <a href="personal.php" class="edit-name">Name Lastname</a>
                    <br>
                    <span class="edit-nickname">@nickname</span>
                </div>
            </div>
        </div>

        <div class="form col-lg-6">
            <form class="edit-form">
                <div class="edit-form-title">Personal Information</div>

                <div class="form-group edit-names">
                    <input type="text" class="form-control edit-form-control" id="firstName" placeholder="First Name">
                    <small class="form-text text-muted">Имя должно быть длинее 3 и короче 30 символов</small>
                    <div id="firstName-danger" style="display:none;" class="text-danger">
                        Неправильно введеное имя
                    </div>
                </div>

                <div class="form-group edit-names">
                    <input type="text" class="form-control edit-form-control"  id="lastName" placeholder="Last Name">
                    <small class="form-text text-muted">Фамилия должна быть длинее 3 и короче 30 символов</small>
                    <div id="lastName-danger" style="display:none;" class="text-danger">
                        Неправильно введеная фамилия
                    </div>
                </div>

                <div class="form-group">
                    <input type="email" class="form-control edit-form-control" id="email" placeholder="Email">
                </div>
                <div id="email-danger" style="display:none;" class="text-danger">
                    Неправильно введенный email
                </div>
                <div class="form-group">
                    <input type="text" class="form-control edit-form-control"  id="username" placeholder="Username">
                    <small class="form-text text-muted">Длинее 3 и короче 15 символов</small>
                </div>
                <div id="username-danger" style="display:none;" class="text-danger">
                    Неправильно введенный username
                </div>
                <div class="form-group">
                    <input type="text" class="form-control edit-form-control" id="bio" placeholder="Bio">
                    <small class="form-text text-muted">Длинее 3 и короче 140 символов</small>
                </div>
                <div id="bio-danger" style="display:none;" class="text-danger">
                    Неправильно введенный bio
                </div>
                <div class="form-group">
                    <input type="text" onfocus="(this.type='date')" class="form-control edit-form-control" id="dateOfBirth"
                           placeholder="Date of Birth">
                </div>
                <div id="dateOfBirth-danger" style="display:none;" class="text-danger">
                    Ошибка
                </div>
                <div class="btn btn-primary edit-btn" id="submitBtn" >UPDATE</div>
            </form>


            <form class="edit-form edit-form-second">
                <div class="edit-form-title">Change Password</div>

                <div class="form-group">
                    <input type="password" id="currentPassword" class="form-control edit-form-control" placeholder="Current Password">
                </div>
                <div id="currentPassword-danger" style="display:none;" class="text-danger">
                    Неправильный пароль
                </div>
                <div class="form-group">
                    <input type="password" id="password" class="form-control edit-form-control" placeholder="Password">
                    <small class="form-text text-muted">Длинее 8 символов, должны быть 2 цыфры, одна заглавная буква</small>
                </div>
                <div id="pass-danger" style="display:none;" class="text-danger">
                    Неправильно введенный пароль
                </div>
                <div class="form-group">
                    <input type="password" id="confirmPassword" class="form-control edit-form-control" placeholder="Confirm Password">
                </div>
                <div id="confirmPassword-danger" style="display:none;" class="text-danger">
                    Пароли не совпадают
                </div>

                <div id="change-pass" class="btn btn-primary edit-btn">CHANGE PASSWORD</div>
            </form>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script src="../js/index.js"></script>
</body>
