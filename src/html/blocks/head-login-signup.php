<?php ?>

<body>

<header class="navbar sticky-top header login-navbar">
    <div class="container">
        <div class="row header-row align-items-center">
            <div class="header-home col-lg-1 d-none d-lg-flex">
                <a class="navigation-item" href="index.php">
                    <img alt="Home icon" class="navigation-item__img home" src="http://bb-twitter-team-3/build/img/home_icon.svg">
                    <span class="navigation-item__text">Home</span>
                </a>
            </div>

            <div class="col-lg-1 d-none d-lg-flex">
                <a class="navigation-item" href="#">
                    <span class="navigation-item__text">About</span>
                </a>
            </div>

            <div class="col-lg-1 offset-lg-3 logo-center">
                <img alt="twitter logo" src="http://bb-twitter-team-3/build/img/twitter_logo.svg" class="login-twitter__logo">
            </div>
        </div>
    </div>
</header>
