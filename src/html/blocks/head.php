<?php ?>

<body>
<header class="navbar sticky-top header">
    <div class="container">
        <div class="row header-row align-items-center">
            <nav class="navbar navbar-expand-md navbar-light justify-content-between">

                <button aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"
                        class="navbar-toggler burger-btn" data-target="#navbarSupportedContent"
                        data-toggle="collapse" type="button">
                    <i class="header-icon fa fa-list-alt fa-4x"></i>
                </button>

                <div class="collapse navbar-collapse navigation-list" id="navbarSupportedContent">
                    <a class="navigation-item d-block d-md-none" href="main-page.php">
                        <span class="navigation-item__text">Home</span>
                    </a>
                    <a class="navigation-item d-block d-md-none" href="search.php">
                        <span class="navigation-item__text">Search</span>
                    </a>
                    <a class="navigation-item d-block d-md-none" href="#">
                        <span class="navigation-item__text">Twitter</span>
                    </a>
                    <a class="navigation-item d-block d-md-none" href="index.php">
                        <span class="navigation-item__text">Logout</span>
                    </a>
                </div>
            </nav>

            <div class="header-home col-lg-1">
                <a class="navigation-item d-none d-md-flex" href="main-page.php">
                    <img alt="Home icon" class="navigation-item__img home" src="../img/home_icon.svg">
                    <span class="navigation-item__text">Home</span>
                </a>
            </div>

            <div class="col-lg-1 offset-lg-5">
                <img alt="twitter logo" src="../img/twitter_logo.svg" class="twitter__logo">
            </div>

            <div class="col-lg-2 offset-1 d-none d-md-block">
                <div class="search-block">
                    <input class="search__input" placeholder="Search" type="text">
                    <a href="search.php"><img alt="Search Icon" class="search__img" src="../img/search_icon.svg"></a>
                </div>
            </div>

            <div class="col-lg-1 d-none d-md-flex align-items-center">
                <a href="#" class="btn btn-primary twit-btn">Twitter</a>
                <a href="index.php" class="nav-link text-muted">Logout</a>
            </div>
        </div>
    </div>
</header>