<?php ?>

<div class="container login-container">
    <form class="login-form">
        <div class="form__title login-form__title">Log in to Twitter</div>
        <div class="form-group login-form-group">
            <input type="email" id="email" class="form-control login-form-control" aria-describedby="emailHelp" placeholder="Email or Username">
        </div>
        <div id="email-danger" style="display:none;" class="text-danger">
            Неправильный email
        </div>
        <div class="form-group login-form-group">
            <input type="password" id="password" class="form-control login-form-control" placeholder="Password">
        </div>
        <div id="pass-danger" style="display:none;" class="text-danger">
            Неправильный пароль
        </div>
        <input type="button" id="submitBtn" class="btn login-btn btn-primary login-btn-primary" value="SUBMIT">
    </form>
    <div class="reference login-reference">
        <div class="reference__text login-reference__text">Need a twitter?
            <a href="signup.php">SIGN UP</a></div>
    </div>

</div>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="../js/index.js"></script>

</body>

