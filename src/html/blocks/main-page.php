<?php ?>

<main class="main">
    <div class="container">
        <div class="row">
            <div class="profile-block-info main-page-profile-block-info col-lg-3">
                <div class="background main-page-background">
                    <div class="profile-img main-page-profile-img"><i
                                class="fas main-page-fas fa-user main-page-fa-user"></i></div>
                </div>
                <div class="card main-page-card">
                    <div class="username main-page-username">
                        <a class="name main-page-name main-page-a" href="personal.php">Yuschyk Yura</a>

                        <div class="nickname">@Yuschyk</div>
                    </div>
                    <div class="info main-page-info">
                        <a class="main-page-a" href="#">
                            Tweets
                            <div class="amount main-page-amount">0</div>
                        </a>
                        <a class="main-page-a" href="#">
                            Following
                            <div class="amount main-page-amount">0</div>
                        </a>
                        <a class="main-page-a" href="#">
                            Followers
                            <div class="amount main-page-amount">0</div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="write-tweet main-page-write-tweet">
                    <form action="" class="tweet-form">
                        <div class="tweet-textarea-block main-page-tweet-textarea-block">
                            <textarea class="textarea main-page-textarea" id="textarea" maxlength="280"
                                      placeholder="Write your tweet..."></textarea>
                            <div class="textarea-counter main-page-textarea-counter"><span class="count-symbol">0</span>/280
                            </div>
                            <div id="textarea-danger" style="display:none;" class="text-danger">
                                Неправильно введенный пароль
                            </div>
                        </div>
                        <div class="tweet-submit-block main-page-tweet-submit-block">
                            <input class="tweet-send main-page-tweet-send" id="submitBtn" type="button" value="TWEET">
                        </div>
                    </form>
                </div>

                <div class="card main-page-card mb-2 mt-4 person-card main-page-person-card">
                    <div class="card-body py-3 px-0 ">

                        <div class="person-info-block main-page-person-info-block">
                            <img class="person-img main-page-person-img mx-3" src="../img/tape/giulioBXProfilePic.png">
                            <span class="px-1">Kyle Kuzma</span>
                            <span class="card-subtitle tweet-time main-page-tweet-time px-3 text-muted font-size-12"> · June 28</span>
                        </div>

                        <p class="card-text mb-3 px-3 font-weight-normal main-page-card-title card-title">Twitter
                            changes its layout,again.
                            This one is a huge tweet.<a class="card-link main-page-card-link main-page-a" href="#">
                                #NewTwitter</a></p>

                        <div class="mt-2 px-3">
                            <div class="tweet-footer main-page-tweet-footer">

                                <div class="mr-2 ">
                                    <img src="../img/tape/replyIcon.png">
                                    <span class=" font-weight-bold font-size-12">150</span>
                                </div>

                                <div class="mr-2 ">
                                    <img src="../img/tape/rtIcon.png">
                                    <span class=" font-weight-bold font-size-12">35</span>
                                </div>

                                <div class="mr-2 ">
                                    <img src="../img/tape/fav_icon_130.png">
                                    <span class=" font-weight-bold font-size-12">50</span>
                                </div>

                                <div class="mr-2 ">
                                    <img src="../img/tape/tweeBox.png">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="card main-page-card mb-2 mt-4 person-card main-page-person-card">
                    <div class="card-body py-3 px-0 ">

                        <div class="person-info-block main-page-person-info-block">
                            <img class="person-img main-page-person-img mx-3" src="../img/user3_profile_pic.png">
                            <span class="px-1">Anthony Davis</span>
                            <span class="card-subtitle tweet-time main-page-tweet-time px-3 text-muted font-size-12"> · Oct 2</span>
                        </div>

                        <p class="card-text mb-3 px-3 font-weight-normal card-title main-page-card-title">This is a
                            tweet with a nice
                            <a class="main-page-a card-link main-page-card-link" href="#"> #picture </a>attached. Feel
                            free to insert your favorite
                            image here.</p>

                        <div class="mt-2 px-3">
                            <div class="tweet-footer main-page-tweet-footer">

                                <div class="mr-2 ">
                                    <img src="../img/tape/replyIcon.png">
                                    <span class=" font-weight-bold font-size-12">121</span>
                                </div>

                                <div class="mr-2 ">
                                    <img src="../img/tape/rtIcon.png">
                                    <span class=" font-weight-bold font-size-12">325</span>
                                </div>

                                <div class="mr-2 ">
                                    <img src="../img/tape/fav_icon_130.png">
                                    <span class=" font-weight-bold font-size-12">7</span>
                                </div>

                                <div class="mr-2 ">
                                    <img src="../img/tape/tweeBox.png">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 followers-block main-page-followers-block">
                <p class="bold main-page-bold follow-description main-page-follow-description">Who to follow · <a
                            class="main-page-a view-all main-page-view-all" href="#viewAll">View all</a></p>

                <div class="follow-person main-page-follow-person">
                    <div class="follow-person-info main-page-follow-person-info">

                        <div class="follow-person-background main-page-follow-person-background">
                            <i class="fas main-page-fas fa-user main-page-fa-user person-icon main-page-person-icon"></i>
                        </div>

                        <div class="person-history">
                            <p class="person-text main-page-person-text">Maksim Zima<span
                                        class="px-2 person-link main-page-person-link">@max</span></p>
                            <input class="person-button main-page-person-button" type="button" value="FOLLOWING">
                        </div>
                    </div>
                    <i class="fas main-page-fas fa-times"></i>
                </div>
                <div class="follow-person main-page-follow-person">
                    <div class="follow-person-info main-page-follow-person-info">

                        <div class="follow-person-background main-page-follow-person-background">
                            <i class="fas main-page-fas fa-user main-page-fa-user person-icon main-page-person-icon"></i>
                        </div>

                        <div class="person-history">
                            <p class="person-text main-page-person-text">Maksim Zima<span
                                        class="px-2 person-link main-page-person-link">@max</span></p>
                            <input class="person-button main-page-person-button" type="button" value="FOLLOWING">
                        </div>
                    </div>
                    <i class="fas main-page-fas fa-times"></i>
                </div>
                <div class="follow-person main-page-follow-person">
                    <div class="follow-person-info main-page-follow-person-info">

                        <div class="follow-person-background main-page-follow-person-background">
                            <i class="fas main-page-fas fa-user main-page-fa-user person-icon main-page-person-icon"></i>
                        </div>

                        <div class="person-history">
                            <p class="person-text main-page-person-text">Maksim Zima<span
                                        class="px-2 person-link main-page-person-link">@max</span></p>
                            <input class="person-button main-page-person-button" type="button" value="FOLLOWING">
                        </div>
                    </div>
                    <i class="fas main-page-fas fa-times"></i>
                </div>

            </div>
        </div>
    </div>
</main>

<script crossorigin="anonymous"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
<script crossorigin="anonymous"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script crossorigin="anonymous"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script src="../js/index.js"></script>
</body>
