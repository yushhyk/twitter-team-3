<?php ?>

<div class="container-fluid personal-background-profile"></div>

<div class="personal-menu-block">
    <div class="container">
        <div class="row">
            <div class="offset-lg-3 col-lg-6 d-lg-flex d-none">
                <div class="personal-tabs">
                    <ul class="personal-tabs__caption">
                        <li class="active personal-tab">Tweets<span class="personal-tabs-number">0</span></li>
                        <li class="personal-tab">Following<span class="personal-tabs-number">2</span></li>
                        <li class="personal-tab">Followers<span class="personal-tabs-number">2</span></li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-3 personal-edit-profile">
                <a class="personal-edit" href="edit.php">Edit Profile</a>

            </div>
        </div>
    </div>
</div>

<main class="main">
    <div class="container">
        <div class="row">

            <div class="col-lg-3">

                <div class="personal-background-avatar">
                    <i class="fas fa-user profile-background-icon"></i>
                </div>


                <div class="personal-info-block">
                    <div class="personal-profile-name">
                        Yuschyk Yura
                    </div>
                    <div class="personal-profile-tag">
                        @Yuschyk
                    </div>

                    <div class="personal-profile-bio">
                        Edit your profile to add your bio
                    </div>
                </div>
            </div>
            <div class="col-lg-6 tabs-content-block">

                <div class="personal-tabs__content active">
                    <div class="personal-write-tweet">
                        <form action="" class="tweet-form">
                            <div class="tweet-textarea-block main-page-tweet-textarea-block">
                            <textarea class="textarea main-page-textarea" id="textarea" maxlength="280"
                                      placeholder="Write your tweet..."></textarea>
                                <div class="textarea-counter main-page-textarea-counter"><span class="count-symbol">0</span>/280
                                </div>
                                <div id="textarea-danger" style="display:none;" class="text-danger">
                                    Неправильно введенный пароль
                                </div>
                            </div>
                            <div class="tweet-submit-block main-page-tweet-submit-block">
                                <input class="tweet-send main-page-tweet-send" id="submitBtn" type="button" value="TWEET">
                            </div>
                        </form>
                    </div>
                </div>

                <div class="personal-tabs__content">
                    <h2>Following</h2>
                </div>

                <div class="personal-tabs__content">
                    <h2>Followers</h2>
                </div>
            </div>

            <div class="col-lg-3 personal-followers-block">
                <p class="personal-follow-description">Who to follow · <a class="personal-view-all" href="#viewAll">View
                        all</a></p>

                <div class="personal-follow-person">
                    <div class="personal-follow-person-info">

                        <div class="personal-follow-person-background">
                            <i class="fas fa-user personal-person-icon"></i>
                        </div>

                        <div class="person-history">
                            <p class="personal-person-text">Maksim Zima<span
                                        class="px-2 personal-person-link">@max</span></p>
                            <input class="personal-person-button" type="button" value="FOLLOWING">
                        </div>
                    </div>
                    <i class="fas fa-times"></i>
                </div>
                <div class="personal-follow-person">
                    <div class="personal-follow-person-info">

                        <div class="personal-follow-person-background">
                            <i class="fas fa-user personal-person-icon"></i>
                        </div>

                        <div class="person-history">
                            <p class="personal-person-text">Maksim Zima<span
                                        class="px-2 personal-person-link">@max</span></p>
                            <input class="personal-person-button" type="button" value="FOLLOWING">
                        </div>
                    </div>
                    <i class="fas fa-times"></i>
                </div>
                <div class="personal-follow-person">
                    <div class="personal-follow-person-info">

                        <div class="personal-follow-person-background">
                            <i class="fas fa-user personal-person-icon"></i>
                        </div>

                        <div class="person-history">
                            <p class="personal-person-text">Maksim Zima<span
                                        class="px-2 personal-person-link">@max</span></p>
                            <input class="personal-person-button" type="button" value="FOLLOWING">
                        </div>
                    </div>
                    <i class="fas fa-times"></i>
                </div>

            </div>
        </div>
    </div>
</main>


<script crossorigin="anonymous"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
<script crossorigin="anonymous"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script crossorigin="anonymous"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script src="../js/index.js"></script>
</body>
