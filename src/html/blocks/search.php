<?php ?>

<div class="container search-main col-lg-10">
    <div class="search-row row">
        <div class="search__profile-block col-lg-3">
            <div class="search-background">
                <div class="search__profile-img"></div>
            </div>
            <div class="search-card">
                <div class="search-username">
                    <a href="personal.php" class="search-name">Name Lastname</a>
                    <br>
                    <span class="search-nickname">@nickname</span>
                </div>
                <div class="search-info">
                    <a href="#" class="search__info-title">
                        Tweets
                        <br>
                        <span class="search__info-amount">0</span>
                    </a>
                    <a href="#" class="search__info-title">
                        Following
                        <br>
                        <span class="search__info-amount">0</span>
                    </a>
                    <a href="#" class="search__info-title">
                        Followers
                        <br>
                        <span class="search__info-amount">0</span>
                    </a>
                </div>
            </div>
        </div>

        <div class="form col-lg-6">

            <input class="form__search" placeholder="Search Twitter" type="text">
            <input type="button" class="form__submit-search" value="">

            <div class="form__search-tape search-row row">
                <div class="form__finder-img mt-2 ml-3"><img src="../img/tape/user.png" alt=""></div>
                <div class="mt-2 ml-2">
                    <p class="form__finder-name">Alex Art</p>
                    <p class="form__finder-nickname">@Alex</p>
                    <input type="button" class="form__finder-following" value="Following">
                </div>
            </div>

            <div class="form__search-tape search-row row">
                <div class="form__finder-img mt-2 ml-3"><img src="../img/tape/user.png" alt=""></div>
                <div class="mt-2 ml-2">
                    <p class="form__finder-name">Alex Art</p>
                    <p class="form__finder-nickname">@Alex</p>
                    <input type="button" class="form__finder-following" value="Following">
                </div>
            </div>
        </div>
    </div>
</div>


<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>

</body>
