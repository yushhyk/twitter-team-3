<?php ?>

<div class="container signup-container">
    <form class="signup-form">
        <div class="form__title signup-form__title">Create an account</div>

        <div class="form-group signup-form-group names signup-names">
            <input type="text" id="firstName" class="form-control signup-form-control" placeholder="First Name">
            <small class="form-text text-muted">Имя должно быть длинее 3 и короче 30 символов</small>
            <div id="firstName-danger" style="display:none;" class="text-danger">
                Неправильно введеное имя
            </div>
        </div>

        <div class="form-group signup-form-group names signup-names">
            <input type="text" id="lastName" class="form-control signup-form-control" placeholder="Last Name">
            <small class="form-text text-muted">Фамилия должна быть длинее 3 и короче 30 символов</small>
            <div id="lastName-danger" style="display:none;" class="text-danger">
                Неправильно введеная фамилия
            </div>
        </div>

        <div class="form-group signup-form-group">
            <input type="email" id="email" class="form-control signup-form-control" placeholder="Email">
        </div>
        <div id="email-danger" style="display:none;" class="text-danger">
            Неправильно введенный email
        </div>
        <div class="form-group signup-form-group">
            <input type="text" id="username" class="form-control signup-form-control" placeholder="Username">
            <small class="form-text text-muted">Длинее 3 и короче 15 символов</small>
        </div>
        <div id="username-danger" style="display:none;" class="text-danger">
            Неправильно введенный username
        </div>
        <div class="form-group signup-form-group">
            <input type="text" id="dateOfBirth" onfocus="(this.type='date')" class="form-control signup-form-control" placeholder="Date of Birth">
        </div>
        <div id="dateOfBirth-danger" style="display:none;" class="text-danger">
            Ошибка
        </div>
        <div class="form-group signup-form-group">
            <input type="password" id="password" class="form-control signup-form-control" placeholder="Password">
            <small class="form-text text-muted">Длинее 8 символов, должны быть 2 цыфры, одна заглавная буква</small>
        </div>
        <div id="pass-danger" style="display:none;" class="text-danger">
            Неправильный пароль
        </div>
        <div class="form-group signup-form-group">
            <input type="password" id="confirmPassword" class="form-control signup-form-control" placeholder="Confirm Password">
        </div>
        <div id="confirmPassword-danger" style="display:none;" class="text-danger">
            Пароли не совпадают
        </div>

        <input type="button" value="SUBMIT" id="submitBtn" class="btn signup-btn btn-primary signup-btn-primary"><a style="text-decoration: none; color: #fff" href="main-page.php"></a>

    </form>
    <div class="reference signup-reference">
        <div class="reference__text signup-reference__text">Already using Twitter?
            <a href="login.php">LOGIN</a></div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" 
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" 
        crossorigin="anonymous"></script>

<script src="../js/index.js"></script>


</body>
