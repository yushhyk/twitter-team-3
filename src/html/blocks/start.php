<?php ?>

<div class="container index-container">
    <img src="http://bb-twitter-team-3/build/img/twitter_bird_logo.svg.png" width="45" height="35">
    <div class="title index-title">See what's happening in the world right now</div>
    <div class="text index-text">Join Twitter today.</div>
    <a href="http://bb-twitter-team-3/build/html/signup.php" class="btn index-btn btn-primary index-btn-primary btn-lg btn-block active" role="button" aria-pressed="true">Sign
        Up</a>
    <a href="http://bb-twitter-team-3/build/html/login.php" class="btn index-btn btn-secondary index-btn-secondary btn-lg btn-block active" role="button" aria-pressed="true">Login</a>
</div>
