<?php ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link crossorigin="anonymous" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" rel="stylesheet">
    <link rel="stylesheet" href="http://bb-twitter-team-3/build/css/styles.css">
    <link href="//abs.twimg.com/favicons/twitter.ico" rel="shortcut icon" type="image/x-icon">
    <title>Entry</title>
</head>
<body>

<?php include ('blocks/start.php'); ?>

</body>
</html>