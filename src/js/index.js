'use strict';

(function($) {
    $(function() {

        $('ul.personal-tabs__caption').on('click', 'li:not(.active)', function() {
            $(this)
                .addClass('active').siblings().removeClass('active')
                .closest('div')
            $(".tabs-content-block").find('div.personal-tabs__content').removeClass('active').eq($(this).index()).addClass('active');
        });

    });
})(jQuery);

const addTwitt = document.getElementById('textarea');
const email = document.getElementById('email');
const userNames = document.getElementById('username');
const firstName = document.getElementById('firstName');
const lastName = document.getElementById('lastName');
const bio = document.getElementById('bio');
const password = document.getElementById('password');
const currentPassword = document.getElementById('currentPassword');
const confirmPassword = document.getElementById('confirmPassword');

const textareaDanger = document.getElementById('textarea-danger');
const emailDanger = document.getElementById('email-danger');
const firstNameDanger = document.getElementById('firstName-danger');
const lastNameDanger = document.getElementById('lastName-danger');
const userNameDanger = document.getElementById('username-danger');
const passDanger = document.getElementById('pass-danger');
const bioDanger = document.getElementById('bio-danger');
const currentPasswordDanger = document.getElementById('currentPassword-danger');
const confirmPasswordDanger = document.getElementById('confirmPassword-danger');


const submitBtn = document.getElementById('submitBtn');
const changePassBtn = document.getElementById('change-pass');

let error = false;
submitBtn.addEventListener('click', checkInput);

function checkInput() {

    if (window.location.href === 'http://bb-twitter-team-3/build/html/main-page.php') {
        error = false;
        if (addTwitt.value.match(/([A-Za-z0-9_\-!@#$%^&*]{3,140})/)) {
            addTwitt.style.border = "0";
            addTwitt.style.borderBottom = "2px solid #DEE2E6";
            textareaDanger.style.display = "none";

        } else {
            addTwitt.style.borderBottom = "2px solid red";
            textareaDanger.style.display = "block";
        }
    } else if (window.location.href === 'http://bb-twitter-team-3/build/html/login.php') {
        error = false;
        if (email.value.match(/^[A-Z0-9._%+-]+@[A-Z0-9-]+.+.[A-Z]{2,4}$/i)) {
            email.style.border = "0";
            email.style.borderBottom = "2px solid #DEE2E6";
            emailDanger.style.display = "none";

        } else {
            error = true;
            email.style.borderBottom = "2px solid red";
            emailDanger.style.display = "block";
        }
        if (password.value.match(/(?=.*[0-9])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,15}/g)) {
            password.style.border = "0";
            password.style.borderBottom = "2px solid #DEE2E6";
            passDanger.style.display = "none";
        } else {
            error = true;
            password.style.borderBottom = "2px solid red";
            passDanger.style.display = "block";
        }
        if (error === false) {
            window.location = 'main-page.php';
        }

    } else if (window.location.href === 'http://bb-twitter-team-3/build/html/signup.php') {
        error = false;
        if (firstName.value.match(/^[A-Za-z-]{3,30}$/)) {
            firstName.style.border = "0";
            firstName.style.borderBottom = "2px solid #DEE2E6";
            firstNameDanger.style.display = "none";
        } else {
            error = true;
            firstName.style.borderBottom = "2px solid red";
            firstNameDanger.style.display = "block";
        }
        if (lastName.value.match(/^[A-Za-z-]{3,30}$/)) {
            lastName.style.border = "0";
            lastName.style.borderBottom = "2px solid #DEE2E6";
            lastNameDanger.style.display = "none";
        } else {
            error = true;
            lastName.style.borderBottom = "2px solid red";
            lastNameDanger.style.display = "block";
        }
        if (email.value.match(/^[A-Z0-9._%+-]+@[A-Z0-9-]+.+.[A-Z]{2,4}$/i)) {
            email.style.border = "0";
            email.style.borderBottom = "2px solid #DEE2E6";
            emailDanger.style.display = "none";

        } else {
            error = true;
            email.style.borderBottom = "2px solid red";
            emailDanger.style.display = "block";
        }
        if (userNames.value.match(/^[A-Za-z-]{3,30}$/)) {
            userNames.style.border = "0";
            userNames.style.borderBottom = "2px solid #DEE2E6";
            userNameDanger.style.display = "none";
        } else {
            error = true;
            userNames.style.borderBottom = "2px solid red";
            userNameDanger.style.display = "block";
        }
        if (password.value.match(/(?=.*[0-9])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,15}/g)) {
            password.style.border = "0";
            password.style.borderBottom = "2px solid #DEE2E6";
            passDanger.style.display = "none";
        } else {
            error = true;
            password.style.borderBottom = "2px solid red";
            passDanger.style.display = "block";
        }
        if (confirmPassword === password) {
            confirmPassword.style.border = "0";
            confirmPassword.style.borderBottom = "2px solid #DEE2E6";
            confirmPasswordDanger.style.display = "none";
        } else {
            error = true;
            confirmPassword.style.borderBottom = "2px solid red";
            confirmPasswordDanger.style.display = "block";
        }
        if (error === false) {
            window.location = 'main-page.html';
        }
    } else if (window.location.href === 'http://bb-twitter-team-3/build/html/personal.php') {
        error = false;
        if (addTwitt.value.match(/([A-Za-z0-9_\-!@#$%^&*]{3,140})/)) {
            addTwitt.style.border = "0";
            addTwitt.style.borderBottom = "2px solid #DEE2E6";
            textareaDanger.style.display = "none";

        } else {
            addTwitt.style.borderBottom = "2px solid red";
            textareaDanger.style.display = "block";
        }
    } else if (window.location.href === 'http://bb-twitter-team-3/build/html/edit.php') {
        error = false;
        if (firstName.value.match(/^[A-Za-z-]{3,30}$/)) {
            firstName.style.border = "0";
            firstName.style.borderBottom = "2px solid #DEE2E6";
            firstNameDanger.style.display = "none";
        } else {
            error = true;
            firstName.style.borderBottom = "2px solid red";
            firstNameDanger.style.display = "block";
        }
        if (lastName.value.match(/^[A-Za-z-]{3,30}$/)) {
            lastName.style.border = "0";
            lastName.style.borderBottom = "2px solid #DEE2E6";
            lastNameDanger.style.display = "none";
        } else {
            error = true;
            lastName.style.borderBottom = "2px solid red";
            lastNameDanger.style.display = "block";
        }
        if (userNames.value.match(/^[A-Za-z-]{3,30}$/)) {
            userNames.style.border = "0";
            userNames.style.borderBottom = "2px solid #DEE2E6";
            userNameDanger.style.display = "none";
        } else {
            error = true;
            userNames.style.borderBottom = "2px solid red";
            userNameDanger.style.display = "block";
        }
        if (email.value.match(/^[A-Z0-9._%+-]+@[A-Z0-9-]+.+.[A-Z]{2,4}$/i)) {
            email.style.border = "0";
            email.style.borderBottom = "2px solid #DEE2E6";
            emailDanger.style.display = "none";

        } else {
            error = true;
            email.style.borderBottom = "2px solid red";
            emailDanger.style.display = "block";
        }
        if (bio.value.match(/\d{3,5}/)) {
            bio.style.border = "0";
            bio.style.borderBottom = "2px solid #DEE2E6";
            bioDanger.style.display = "none";
        } else {
            error = true;
            bio.style.borderBottom = "2px solid red";
            bioDanger.style.display = "block";
        }
    }

}
if (window.location.href === 'http://bb-twitter-team-3/build/html/edit.php') {
    error = false;
    changePassBtn.addEventListener('click', changePass);

    function changePass() {
        if (currentPassword.value.match(/(?=.*[0-9])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,15}/g)) {
            currentPassword.style.border = "0";
            currentPassword.style.borderBottom = "2px solid #DEE2E6";
            currentPasswordDanger.style.display = "none";
        } else {
            error = true;
            currentPassword.style.borderBottom = "2px solid red";
            currentPasswordDanger.style.display = "block";
        }
        if (password.value.match(/(?=.*[0-9])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,15}/g)) {
            password.style.border = "0";
            password.style.borderBottom = "2px solid #DEE2E6";
            passDanger.style.display = "none";
        } else {
            error = true;
            password.style.borderBottom = "2px solid red";
            passDanger.style.display = "block";
        }
        if (password.value === confirmPassword.value) {
            confirmPassword.style.border = "0";
            confirmPassword.style.borderBottom = "2px solid #DEE2E6";
            confirmPasswordDanger.style.display = "none";
        } else {
            error = true;
            confirmPassword.style.borderBottom = "2px solid red";
            confirmPasswordDanger.style.display = "block";
        }
    }
}




